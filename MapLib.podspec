

Pod::Spec.new do |s|


  s.name         = "MapLib"
  s.version      = "1.0.0"
  s.summary      = "MapLib framework"
  s.description  = "MapLib framework"
  s.homepage     = "http://EXAMPLE/MapLib"
  s.license      = "MapLib"
  s.author       = { "Iurii Voskresenskii" => "" }
  s.module_name = 'MapLib'

	s.ios.deployment_target = "10.0"
	s.osx.deployment_target = "10.7"
	s.watchos.deployment_target = "2.0"
	s.tvos.deployment_target = "9.0"


  s.source       = { :git => 'https://bitbucket.org/yvoskresenskiy/maplib', :branch => 'master'}

  s.source_files  = "MapLib/**/*.{swift, h, m}"

  s.frameworks = 'Foundation', 'CoreGraphics', 'CoreLocation', 'CoreBluetooth', 'QuartzCore', 'GLKit', 'OpenGLES', 'Security', 'ImageIO', 'AdSupport', 'CoreData', 'CoreImage', 'MapKit', 'Accelerate', 'SystemConfiguration', 'UIKit'

  s.libraries = 'c++', 'c', 'z'
  s.resource = "Resources/MapBundle.bundle"
  s.resources = [ "MapLib/resources/MapResources.bundle"]

#s.dependency 'Google-Maps-iOS-Utils'
  s.dependency 'GoogleMaps'

end
