//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//
import Foundation
import CoreLocation


public protocol MapCoordinatesBounds
{
    var nearLeft: CLLocationCoordinate2D { get }
    var farRight: CLLocationCoordinate2D { get }
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
}
