//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation

public struct TileCell
{
    public let column: UInt
    public let row: UInt
    public let zoom: UInt
    
    public init(column: UInt, row: UInt, zoom: UInt) {
        self.column = column
        self.row = row
        self.zoom = zoom
    }
    
    // monotonically increasing function
    private func longitudeOfColumn(_ column: UInt) -> Double
    {
        return Double(column) / pow(2.0, Double(zoom)) * 360.0 - 180.0
    }
    
    // monotonically decreasing function
    private func latitudeOfRow(_ row: UInt) -> Double
    {
        let value: Double = Double.pi - 2.0 * Double.pi * Double(row) / pow(2.0, Double(zoom))
        return 180.0 / Double.pi * atan(0.5 * (exp(value) - exp(-value)))
    }
    
    public var rect: MapRect
    {
        get {
            let nearLeft = CLLocationCoordinate2D(latitude: latitudeOfRow(row+1), longitude: longitudeOfColumn(column))
            let farRight = CLLocationCoordinate2D(latitude: latitudeOfRow(row), longitude: longitudeOfColumn(column+1))
            
            return MapRect(nearLeft: nearLeft, farRight: farRight)
        }
    }
    
    
}
