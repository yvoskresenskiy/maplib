//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

public protocol MapViewDelegate: class
{
    func mapView(_ mapView: MapView!, idleAtCameraPosition position: MapCameraPosition!);
    func mapView(_ mapView: MapView!, didTapMarker marker: MapMarker!) -> Bool;
    func mapView(_ mapView: MapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D)
    func mapView(_ mapView: MapView!, didLongPressAt coordinate: CLLocationCoordinate2D)
    func mapView(_ mapView: MapView!, didChangeCameraPosition position: CLLocationCoordinate2D)
    func mapView(_ mapView: MapView, didEndDragging marker: MapMarker)
}

public protocol MapView : class
{
    var view: UIView { get }
    
    weak var delegate:MapViewDelegate? { get set }
    var projection: MapProjection { get }
    var cameraPosition:MapCameraPosition { get }
    var customPopupViewHandler: ((MapMarker) -> (UIView?))? { get set }

    func myLocation() -> CLLocation!;
    func setMyLocationEnabled(_ enabled: Bool);
    func clean();
    
    func focusMyLocation()
    
    func addMarker(_ marker:MapMarker)
    func addOverlay(_ overlay: MapOverlay)
    func addTileLayer(_ tileLayer: MapTileLayer)
    func removeMarker(_ marker:MapMarker)
    func clusterItems()
    
    func buildPath(_ mapPath: MapPath, strokeColor: UIColor, strokeWidth: CGFloat)
    
    func handleMyLocation(_ completion: @escaping (_ locationCoordinates: CLLocationCoordinate2D)->(Void));
    func stopHandleMyLocation();
    
    func zoomIn();
    func zoomOut();
    func currentZoom() -> Float
    
    func animateToCurrentLocation();
    func animateToLocation(_ location: CLLocationCoordinate2D, zoom: Float)
    func animateToZoom(_ zoom: Float);
    
    func updateCameraPadding(_ padding: UIEdgeInsets)
}
