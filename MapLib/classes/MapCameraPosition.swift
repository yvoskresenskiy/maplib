//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics

public protocol MapCameraPosition
{    
     var target: CLLocationCoordinate2D { get }
     var zoom: Float { get }
     var bearing: CLLocationDirection { get }
     var viewingAngle: Double { get }
    
    static func zoomAtCoordinate(_ coordinate: CLLocationCoordinate2D, forMeters meters: CLLocationDistance, perPoints points: CGFloat) -> Float;
    
}

