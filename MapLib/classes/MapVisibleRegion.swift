//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation


// rectangle area on the map
public struct MapRect
{
    public var nearLeft: CLLocationCoordinate2D
    public var farRight: CLLocationCoordinate2D
    
    public init(nearLeft: CLLocationCoordinate2D, farRight: CLLocationCoordinate2D)
    {
        self.nearLeft = nearLeft
        self.farRight = farRight
    }
    
    public func containsCoordinate(_ point: CLLocationCoordinate2D) -> Bool
    {
        return
            point.latitude  >= nearLeft.latitude  &&
                point.latitude  <= farRight.latitude &&
                point.longitude >= nearLeft.longitude &&
                point.longitude <= farRight.longitude
    }
}


public protocol MapVisibleRegion
{
    var nearLeft:CLLocationCoordinate2D  { get }
    var nearRight:CLLocationCoordinate2D { get }
    var farLeft:CLLocationCoordinate2D { get }
    var farRight:CLLocationCoordinate2D { get }
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
    func getCoveredBounds() -> MapCoordinatesBounds
}


extension MapVisibleRegion
{
    fileprivate func min4(_ a: Double, _ b: Double, _ c: Double, _ d: Double) -> Double {
        return min(min(a,b), min(c,d))
    }
    
    fileprivate func max4(_ a: Double, _ b: Double, _ c: Double, _ d: Double) -> Double {
        return max(max(a,b), max(c,d))
    }
    
    // Coordinate rectangle is a rectange each side of which is parallels to the Equator or to the Greenwich meridian.
    // Returns rectangle which includes MapVisibleRegion polygon with the smallect square
    func getSmallestCoordinateRect() -> MapRect
    {
        let minLat, minLong, maxLat, maxLong: Double
        
        minLat = min4(nearLeft.latitude, nearRight.latitude, farLeft.latitude, farRight.latitude)
        minLong = min4(nearLeft.longitude, nearRight.longitude, farLeft.longitude, farRight.longitude)
        maxLat = max4(nearLeft.latitude, nearRight.latitude, farLeft.latitude, farRight.latitude)
        maxLong = max4(nearLeft.longitude, nearRight.longitude, farLeft.longitude, farRight.longitude)
        
        let rectNearLeft = CLLocationCoordinate2D(latitude: minLat, longitude: minLong)
        let rectFarRightRect = CLLocationCoordinate2D(latitude: maxLat, longitude: maxLong)
        
        return MapRect(nearLeft: rectNearLeft, farRight: rectFarRightRect)
    }
}

