//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//


import Foundation
import CoreLocation
import CoreGraphics
import UIKit

public protocol MapPath : class
{
    func count() -> UInt
    func coordinateAtIndex(_ index: UInt) -> CLLocationCoordinate2D
    func addCoordinate(_ coord: CLLocationCoordinate2D)
}

