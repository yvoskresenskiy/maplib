//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import UIKit


public protocol MapTileLayer
{
    func removeFromMap()
    weak var delegate: MapTileLayerDelegate? { get set }
    func clearTileCache()
    var sizeOfTile: CGFloat { get set } // size of tile in pt
}


public protocol MapTileLayerDelegate: class
{
    func requestTileFor(cell: TileCell, showImage: @escaping (UIImage?)->Void)
    func clearTileCache()
}

