//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics

public protocol MapProjection
{
    func pointForCoordinate(_ coordinate: CLLocationCoordinate2D) -> CGPoint
    func coordinateForPoint(_ point: CGPoint) -> CLLocationCoordinate2D
    func pointsForMeters(_ meters: CLLocationDistance, atCoordinate coordinate: CLLocationCoordinate2D) -> CGFloat
    
  
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
    
   
    func visibleRegion() -> MapVisibleRegion;
}
