//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation

public enum CameraConstants: Double
{
    case startLatitude = 55.753744 // Moscow Red Square
    case startLongtitude = 37.621253
    case zoom = 13.5 // zoom lvl for worst accuracy detection
    case bearing = 30.0
    case viewingAngle = 45.0
}
