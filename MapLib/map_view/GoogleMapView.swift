//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import QuartzCore


public enum ZoomLevelConst: Float
{
    case max = 22.0
    case min = 2.0
    case step = 1.0
}

open class GoogleMapView: UIView, MapView, GMSMapViewDelegate, GMUClusterManagerDelegate
{
    
    open var view: UIView
    {
        return self
    }
    
    open var mapView:GMSMapView!;
    private var clusterManager: GMUClusterManager!

    open weak var delegate:MapViewDelegate?
    
    fileprivate var locationObserver:((_ locationCoordinates: CLLocationCoordinate2D)->(Void))?;
    
    //MARK: - userPosition
    
    fileprivate var userPosition: CLLocation?
    
    //MARK: - focus on launch flag
    
    var isFocusedPosition: Bool = false
    
    open override var frame: CGRect
    {
        didSet
        {
            if mapView != nil
            {
                mapView.frame = frame
            }
        }
    }
    
    public var customPopupViewHandler: ((MapMarker) -> (UIView?))?
    
    var isDraggingInProgress: Bool = false
    
    open var projection: MapProjection
    {
        return MapProjectionBuilder.buildGoogleMapProjection(self.mapView.projection);
    }
    
    open var cameraPosition: MapCameraPosition
    {
        return MapCameraPositionBuilder.buildGoogleCameraPosition(self.mapView.camera);
    }
    
    //MARK: Memory managment
    override init(frame: CGRect)
    {
        super.init(frame: frame);
        self.baseInit()
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib()
    {
        self.baseInit()
    }
    
    var pulseAnimationTimer: Timer?
    
    func baseInit()
    {
        let camera = GMSCameraPosition.camera(withLatitude: CameraConstants.startLatitude.rawValue,
                                              longitude: CameraConstants.startLongtitude.rawValue,
                                              zoom: Float(CameraConstants.zoom.rawValue),
                                              bearing: 0,
                                              viewingAngle: 0)

        mapView = GMSMapView.map(withFrame: self.bounds, camera: camera)
        self.addSubview(self.mapView);
        configureMap()

        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    deinit
    {
        self.stopHandleMyLocation()
    }
    
    open override func layoutSubviews()
    {
        super.layoutSubviews()
        self.mapView.frame = self.bounds
    }
    
    //MARK: - UI
    
    fileprivate func configureMap()
    {
        mapView.settings.setAllGesturesEnabled(true)
        mapView.delegate = self
        mapView.isIndoorEnabled = false
        mapView.settings.compassButton = false
        mapView.settings.indoorPicker = false
        mapView.settings.myLocationButton = true
        mapView.settings.rotateGestures = false
        mapView.settings.tiltGestures = false
        mapView.isMyLocationEnabled = true
    }
    
    func clusterItems() {
        clusterManager.cluster()
    }
    
    //MARK: Location
    open func setMyLocationEnabled(_ enabled: Bool)
    {
        mapView.isMyLocationEnabled = enabled
    }
    
    open func myLocation() -> CLLocation!
    {
        return mapView.myLocation
    }
    
    //MARK: Markers
    
    open func addMarker(_ marker:MapMarker)
    {
        if let googleMarker = marker as? GoogleMapMarker
        {
            googleMarker.map = self.mapView
            clusterManager.add(googleMarker)
        }
    }
    
    public func focusMyLocation()
    {
        if let location = mapView.myLocation
        {
            self.mapView.animate(toLocation: location.coordinate)
        }
    }
    
    open func removeMarker(_ marker:MapMarker)
    {
        if let googleMarker = marker as? GoogleMapMarker
        {
            googleMarker.map = nil;
        }
    }
    
    open func clean() {
        mapView.clear()
    }
    
    open func addOverlay(_ overlay: MapOverlay)
    {
        if let googleOverlay = overlay as? GoogleMapOverlay
        {
            googleOverlay.map = self.mapView
        }
    }
    
    public func addTileLayer(_ tileLayer: MapTileLayer)
    {
        if let googleTileLayer = tileLayer as? GoogleMapTileLayer
        {
            googleTileLayer.map = self.mapView
        }
    }
    
    //MARK: Zoom
    open func zoomIn()
    {
        let zoomLevel = (self.mapView?.camera.zoom)! + ZoomLevelConst.step.rawValue
        
        if (zoomLevel > ZoomLevelConst.max.rawValue)
        {
            self.mapView.animate(toZoom: ZoomLevelConst.max.rawValue)
        }
        else
        {
            self.mapView.animate(toZoom: zoomLevel)
        }
    }
    
    open func zoomOut()
    {
        let zoomLevel = (self.mapView?.camera.zoom)! - ZoomLevelConst.step.rawValue
        
        if (zoomLevel > ZoomLevelConst.min.rawValue)
        {
            self.mapView.animate(toZoom: zoomLevel)
        }
        else
        {
            self.mapView.animate(toZoom: ZoomLevelConst.min.rawValue)
        }
    }
    
    open func currentZoom() -> Float {
        guard let mapView = self.mapView else {
            return Float(CameraConstants.zoom.rawValue)
        }
        return mapView.camera.zoom
    }
    
    //MARK: GMSMapViewDelegate
    open func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    }
    
    
    open func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition)
    {
        delegate?.mapView(self, idleAtCameraPosition: MapCameraPositionBuilder.buildGoogleCameraPosition(cameraPosition))
    }
    
    open func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        delegate?.mapView(self, didChangeCameraPosition: position.target)
    }
    
    open func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        
        if delegate != nil
        {
            mapView.selectedMarker = marker
            return delegate!.mapView(self, didTapMarker: MapMarkerBuilder.buildGoogleMarker(marker))
        }
        return true
        
    }
    
    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        if !isDraggingInProgress {
            delegate?.mapView(self, didLongPressAt: coordinate)
        }
    }
    
    open func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        delegate?.mapView(self, didTapAtCoordinate: coordinate)
    }
    
    open func buildPath(_ mapPath: MapPath,strokeColor: UIColor = UIColor.black,strokeWidth: CGFloat = 2.0)
    {
        if let gmsPath = (mapPath as? GoogleMapPath)?.path {
            let mPolyline = GMSPolyline(path: gmsPath)
            mPolyline.strokeColor = strokeColor
            mPolyline.strokeWidth = strokeWidth
            mPolyline.map = self.mapView
        }
    }
    
    public func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        isDraggingInProgress = true
    }
    
    public func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        isDraggingInProgress = false
        delegate?.mapView(self, didEndDragging: MapMarkerBuilder.buildGoogleMarker(marker))
    }
    
    public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView?
    {
        guard let customViewHandler = self.customPopupViewHandler else
        {
            return nil
        }
        
        return customViewHandler(MapMarkerBuilder.buildGoogleMarker(marker))
    }
    
    //MARK: Observer
    
    open func handleMyLocation(_ completion:@escaping (_ locationCoordinates: CLLocationCoordinate2D)->(Void))
    {
        locationObserver = completion
        let options = NSKeyValueObservingOptions([.new])
        self.mapView?.addObserver(self, forKeyPath: "myLocation", options: options, context: nil)
    }
    
    open func stopHandleMyLocation()
    {
        if locationObserver != nil {
            self.mapView.removeObserver(self, forKeyPath: "myLocation", context: nil)
            locationObserver = nil;
        }
    }
    
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let newValue = change?[NSKeyValueChangeKey.newKey] {
            if locationObserver != nil && (newValue as AnyObject).coordinate != nil {
                locationObserver!((newValue as AnyObject).coordinate)
            }
        }
    }
    
    open func animateToZoom(_ zoom: Float) {
        self.mapView.animate(toZoom: zoom)
    }
    
    open func animateToCurrentLocation()
    {
        if self.mapView.myLocation != nil
        {
            self.mapView.animate(toLocation: (self.mapView?.myLocation?.coordinate)!)
            self.mapView.animate(toZoom: 16.0)
        }
    }
    
    open func animateToLocation(_ location: CLLocationCoordinate2D, zoom: Float)
    {
        let position = GMSCameraPosition(target: location, zoom: zoom, bearing: self.mapView.camera.bearing, viewingAngle: self.mapView.camera.viewingAngle)
        self.mapView.animate(to: position)
    }
    
    open func updateCameraPadding(_ padding: UIEdgeInsets)
    {
        mapView.padding = padding
    }
    
    // MARK: - GMUClusterManagerDelegate
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool
    {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return false
    }
}

public func mapProject(_ point: CLLocationCoordinate2D) -> MapPoint {
    let point: GMSMapPoint = GMSProject(point)
    return MapPoint(x: point.x, y: point.y)
}
