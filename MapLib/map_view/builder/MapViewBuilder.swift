//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import QuartzCore


open class MapViewBuilder
{
    open static func buildMapView (_ frame:CGRect) -> MapView
    {
        return GoogleMapView(frame: frame);
    }
}
