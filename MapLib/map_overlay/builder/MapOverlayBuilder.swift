//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


open class MapOverlayBuilder
{
    open static func buildOverlay(_ bounds: MapCoordinatesBounds, icon: UIImage?) -> MapOverlay
    {
        let gmsbounds = GMSCoordinateBounds(coordinate: bounds.nearLeft, coordinate: bounds.farRight)
        return GoogleMapOverlay(bounds: gmsbounds, icon: icon)
    }
    
}
