//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


class GoogleMapOverlay: GMSGroundOverlay, MapOverlay {
    
    func removeFromMap()
    {
        self.map = nil
    }

}
