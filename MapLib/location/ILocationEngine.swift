//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation

public protocol ILocationEngine: class {
    
    var locationMonitoring:LocationMonitoring { get }
    var locationSearch:LocationSearch { get }
    var locationPermission:LocationPermission { get }
}
