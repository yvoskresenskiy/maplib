//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation

public protocol LocationPermission {
 
    func authorizationStatus() -> CLAuthorizationStatus;
    func locationServiceEnable() -> Bool;
    func locationServiceAvailable() -> Bool;
    
    func handleChangePermission(_ completion: @escaping (_ status:CLAuthorizationStatus)->(Void));
    func requestPermission(_ completion: @escaping (_ status:CLAuthorizationStatus)->(Void));
}
