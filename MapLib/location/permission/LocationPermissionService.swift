//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation

open class LocationPermissionService: NSObject, LocationPermission, CLLocationManagerDelegate  {
    
    fileprivate var mLocationManager:CLLocationManager;
    fileprivate var mChangePermissionBlock:((_ status: CLAuthorizationStatus) -> (Void))?;
    fileprivate var mHandleChangePermissionBlock:((_ status: CLAuthorizationStatus) -> (Void))?;
    
    
    public override init()
    {
        mLocationManager = CLLocationManager()
        super.init();
        mLocationManager.delegate = self;
    }
    
    
    //MARK: LocationPermission
    open func authorizationStatus() -> CLAuthorizationStatus
    {
        return CLLocationManager.authorizationStatus();
    }
    
    open func locationServiceEnable() -> Bool
    {
        return CLLocationManager.locationServicesEnabled()
    }
    
    open func locationServiceAvailable() -> Bool
    {
        var serviceAvailable:Bool = false;
        
        if self.locationServiceEnable() == true &&
            (self.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || self.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse)
        {
            serviceAvailable = true;
        }
        
        return serviceAvailable;
    }
    
    open func handleChangePermission(_ completion:@escaping (_ status:CLAuthorizationStatus)->(Void))
    {
        mHandleChangePermissionBlock = completion;
    }
    
    open func requestPermission(_ completion:@escaping (_ status:CLAuthorizationStatus)->(Void))
    {
        mChangePermissionBlock = completion;
        mLocationManager.requestWhenInUseAuthorization()
    }
    
    //MARK: CLLocationManagerDelegate
    
    open func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (mHandleChangePermissionBlock != nil){
            mHandleChangePermissionBlock!(status);
        }
        
        if (mChangePermissionBlock != nil)
        {
            mChangePermissionBlock!(status);
            mChangePermissionBlock = nil;
        }
        
    }
    
}
