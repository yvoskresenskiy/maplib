//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation

open class LocationEngine: ILocationEngine {
 
    fileprivate var mLocationMonitoring:LocationMonitoring
    fileprivate var mLocationSearch:LocationSearch
    fileprivate var mLocationPermission:LocationPermission
    
    public init() {
        mLocationSearch = LocationSearchService()
        mLocationMonitoring = LocationMonitoringService()
        mLocationPermission = LocationPermissionService()
    }
    
    //MARK: ILocationEngine
    open var locationMonitoring:LocationMonitoring {
        return mLocationMonitoring;
    }
    
    open var locationSearch:LocationSearch {
        return mLocationSearch;
    }
    
    open var locationPermission:LocationPermission {
        return mLocationPermission;
    }
}
