//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation


open class LocationMonitoringService: NSObject, CLLocationManagerDelegate, LocationMonitoring {
    
    fileprivate var mLocationManager:CLLocationManager;
    fileprivate var handleEnterRegionBlock:((_ manager: CLLocationManager, _ region: CLRegion)->(Void))?;
    fileprivate var handleExitRegionBlock:((_ manager: CLLocationManager, _ region: CLRegion)->(Void))?;
    fileprivate var handleUpdateLocationBlock:((_ locationCoordinates: CLLocation)->(Void))?;
    
    fileprivate let DistanceFilterMeters = 100.0
    
    public override init()
    {
        mLocationManager = CLLocationManager()
        super.init();
        mLocationManager.delegate = self;
        mLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        mLocationManager.distanceFilter = DistanceFilterMeters
    }
    
    //MARKL: LocationMonitoring
    open func startMonitoring(_ region:CLCircularRegion)
    {
        mLocationManager.startMonitoring(for: region);
    }
    
    open func stopMonitoring(_ region:CLCircularRegion)
    {
        mLocationManager.stopMonitoring(for: region);
    }
    
    open func stopMonitoringAllRegions()
    {
        for region in mLocationManager.monitoredRegions
        {
            mLocationManager.stopMonitoring(for: region);
        }
    }
    
    open func startUpdatingLocation() {
        mLocationManager.startUpdatingLocation()
    }
    
    open func stopUpdatingLocation() {
        mLocationManager.stopUpdatingLocation()
    }
    
    open func handleUpdateLocation(_ completion:@escaping (_ locationCoordinates: CLLocation)->(Void))
    {
        self.handleUpdateLocationBlock = completion
    }
    
    open func handleEnterRegion(_ completion:@escaping (_ manager: CLLocationManager, _ region: CLRegion)->(Void))
    {
        self.handleEnterRegionBlock = completion;
    }
    
    open func handleExitRegion(_ completion:@escaping (_ manager: CLLocationManager,  _ region: CLRegion)->(Void))
    {
        self.handleExitRegionBlock = completion;
    }
    
    //MARK: CLLocationManagerDelegate
    
    open func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (self.handleUpdateLocationBlock != nil) {
            if let location = locations.last
            {
               self.handleUpdateLocationBlock!(location)
            }
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion)
    {
        if (self.handleEnterRegionBlock != nil)
        {
            self.handleEnterRegionBlock!(manager, region);
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion)
    {
        if (self.handleExitRegionBlock != nil)
        {
            self.handleExitRegionBlock!(manager, region);
        }
    }
}
