//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation

public protocol LocationMonitoring {
    
    func startMonitoring(_ region:CLCircularRegion);
    func stopMonitoring(_ region:CLCircularRegion);
    func stopMonitoringAllRegions();
    func startUpdatingLocation();
    func stopUpdatingLocation();
    
    func handleEnterRegion(_ completion: @escaping (_ manager: CLLocationManager, _ region: CLRegion)->(Void));
    func handleExitRegion(_ completion: @escaping (_ manager: CLLocationManager,  _ region: CLRegion)->(Void));
    func handleUpdateLocation(_ completion: @escaping (_ locationCoordinates: CLLocation)->(Void));
}
