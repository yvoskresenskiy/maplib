//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation

public protocol LocationSearch {
  
    func requestLocation(_ completion: @escaping (_ location:CLLocation?, _ error:NSError?)->(Void));
}
