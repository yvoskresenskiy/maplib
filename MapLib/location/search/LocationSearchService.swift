//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

open class LocationSearchService: NSObject, CLLocationManagerDelegate, LocationSearch {

    fileprivate var mLocationManager:CLLocationManager;
    fileprivate var updateLocationBlock:((_ location:CLLocation?, _ error:NSError?) -> (Void))?;
    
    public override init()
    {
        mLocationManager = CLLocationManager()
        super.init();
        mLocationManager.delegate = self;
    }
    
    public func requestLocation(_ completion: @escaping (CLLocation?, NSError?) -> (Void))
    {
        self.updateLocationBlock = completion;
        mLocationManager.startUpdatingLocation();
    }
    
    //MARK: CLLocationManagerDelegate
    
    open func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (self.updateLocationBlock != nil ){
            
            self.updateLocationBlock!(locations.first, nil);
            self.updateLocationBlock = nil;
        }
    }
    
}
