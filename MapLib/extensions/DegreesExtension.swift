//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation

extension Int
{
    var degreesToRadians: Double { return Double(self) * Double.pi / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / Double.pi }
}

extension Double
{
    var degreesToRadians: Double { return self * Double.pi / 180 }
    var radiansToDegrees: Double { return self * 180 / Double.pi }
}
