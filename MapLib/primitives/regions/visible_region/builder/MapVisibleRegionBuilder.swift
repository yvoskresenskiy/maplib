//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps

open class MapVisibleRegionBuilder
{
    open static func buildGoogleVisibleRegion(_ gmsVisibleRegion:GMSVisibleRegion) -> MapVisibleRegion
    {
        let visibleRegion:GoogleMapVisibleRegion = GoogleMapVisibleRegion(nearLeft: gmsVisibleRegion.nearLeft, nearRight: gmsVisibleRegion.nearRight, farLeft: gmsVisibleRegion.farLeft, farRight: gmsVisibleRegion.farRight);
        
        return visibleRegion;
    }
    
    open static func buildVisibleRegion(_ nearLeft: CLLocationCoordinate2D, nearRight: CLLocationCoordinate2D,farLeft: CLLocationCoordinate2D,farRight: CLLocationCoordinate2D) -> MapVisibleRegion
    {
        let visibleRegion:GoogleMapVisibleRegion = GoogleMapVisibleRegion(nearLeft: nearLeft, nearRight: nearRight, farLeft: farLeft, farRight: farRight);
        
        return visibleRegion;
    }
    
    open static func buildVisibleRegion(center coordinate: CLLocationCoordinate2D, distance: CLLocationDistance) -> MapVisibleRegion
    {
        let nearRight = buildCoordinate(coordinate, distance: distance, heading: 135)
        let farLeft = buildCoordinate(coordinate, distance: distance, heading: 315)
        let nearLeft = CLLocationCoordinate2D(latitude: nearRight.latitude, longitude: farLeft.longitude)
        let farRight = CLLocationCoordinate2D(latitude: farLeft.latitude, longitude: nearRight.longitude)
        let visibleRegion:GoogleMapVisibleRegion = GoogleMapVisibleRegion(nearLeft: nearLeft, nearRight: nearRight, farLeft: farLeft, farRight: farRight);
        return visibleRegion
    }
    
    fileprivate static func buildCoordinate(_ coordinate: CLLocationCoordinate2D, distance: CLLocationDistance, heading: CLLocationDirection) -> CLLocationCoordinate2D
    {
        return GMSGeometryOffset(coordinate, distance, heading)
    }
}
