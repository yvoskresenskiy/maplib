//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps

class GoogleMapVisibleRegion : MapVisibleRegion
{
    var nearLeft:CLLocationCoordinate2D
    var nearRight:CLLocationCoordinate2D
    var farLeft:CLLocationCoordinate2D
    var farRight:CLLocationCoordinate2D
    
    init(nearLeft:CLLocationCoordinate2D, nearRight:CLLocationCoordinate2D, farLeft:CLLocationCoordinate2D, farRight:CLLocationCoordinate2D)
    {
        self.nearLeft = nearLeft;
        self.nearRight = nearRight;
        self.farLeft = farLeft;
        self.farRight = farRight;
    }
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
    {
        // TODO temporarly decision (first aproximation):
        return getCoveredBounds().containsCoordinate(coordinate)
    }
    
    func getCoveredBounds() -> MapCoordinatesBounds
    {
        let maxLatitude = max(farLeft.latitude, farRight.latitude)
        let minLatitude = min(nearLeft.latitude, nearRight.latitude)
        
        let maxLongitude = max(farRight.longitude, nearRight.longitude)
        let minLongitude = min(farLeft.longitude, nearLeft.longitude)
        
        let minPoint = CLLocationCoordinate2D(latitude: minLatitude, longitude: minLongitude)
        let maxPoint = CLLocationCoordinate2D(latitude: maxLatitude, longitude: maxLongitude)
        
        return MapCoordinatesBoundsBuilder.buildCoordinatesBounds(coordinate: minPoint, coordinate: maxPoint)
    }
}
