//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


public class GoogleMapMarker: GMSMarker, MapMarker, GMUClusterItem {
 
    public func markerImageWithColor(_ color: UIColor?) -> UIImage {
        return GMSMarker.markerImage(with: color);
    }
    
    public var markerLayer: MapMarkerLayer
    {
        get {
            return self.layer
        }
    }
    
    public func removeFromMap()
    {
        self.map = nil
    }

    public var id: String?
}
