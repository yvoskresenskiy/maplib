//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


open class MapMarkerBuilder
{
    internal static func buildGoogleMarker(_ gmsMarker:GMSMarker) -> MapMarker
    {
        let marker:GoogleMapMarker = GoogleMapMarker();
       
        marker.position = gmsMarker.position;
        marker.snippet = gmsMarker.snippet ;
        marker.icon = gmsMarker.icon;
        marker.iconView = gmsMarker.iconView;
        marker.tracksViewChanges = gmsMarker.tracksViewChanges;
        marker.tracksInfoWindowChanges = gmsMarker.tracksInfoWindowChanges;
        marker.groundAnchor = gmsMarker.groundAnchor;
        marker.infoWindowAnchor = gmsMarker.infoWindowAnchor;
        marker.isDraggable = gmsMarker.isDraggable;
        marker.isFlat = gmsMarker.isFlat;
        marker.rotation = gmsMarker.rotation;
        marker.opacity = gmsMarker.opacity;
        marker.userData = gmsMarker.userData;
        
        return marker;
    }
    
    open static func buildGoogleMarker(_ coordinates:CLLocationCoordinate2D?, title: String, userData:Any?, icon: UIImage?) -> MapMarker
    {
        var marker:GoogleMapMarker? = nil;
        
        if coordinates != nil
        {
            marker = GoogleMapMarker(position: coordinates!)
        }
        else
        {
            marker = GoogleMapMarker()
        }
        marker?.title = title;
        marker?.userData = userData;
        marker?.icon = icon
        return marker!;
    }
    
    open static func buildDraggableGoogleMarker(_ coordinate: CLLocationCoordinate2D?,
                                                title: String?,
                                                userData: Any?,
                                                icon: UIImage?) -> MapMarker {
        var marker: GoogleMapMarker
        
        if coordinate != nil {
            marker = GoogleMapMarker(position: coordinate!)
        }
        else {
            marker = GoogleMapMarker()
        }
        marker.title       = title
        marker.userData    = userData
        marker.icon        = icon
        marker.isDraggable = true
        return marker
    }
}
