//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps

class GoogleMapProjection: MapProjection {
 
    fileprivate let projection:GMSProjection;
    
    required init(projection:GMSProjection){
        self.projection = projection;
    }
    
    func pointForCoordinate(_ coordinate: CLLocationCoordinate2D) -> CGPoint{
        return self.projection.point(for: coordinate);
    }
    
    func coordinateForPoint(_ point: CGPoint) -> CLLocationCoordinate2D{
        return self.projection.coordinate(for: point);
    }
    
    func pointsForMeters(_ meters: CLLocationDistance, atCoordinate coordinate: CLLocationCoordinate2D) -> CGFloat{
        return self.projection.points(forMeters: meters, at: coordinate);
    }
    
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool{
        return self.projection.contains(coordinate);
    }
    
    func visibleRegion() -> MapVisibleRegion{
        return MapVisibleRegionBuilder.buildGoogleVisibleRegion(self.projection.visibleRegion());
    }
}
