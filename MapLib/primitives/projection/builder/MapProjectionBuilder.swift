//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps

open class MapProjectionBuilder
{
    static func buildGoogleMapProjection(_ projection:GMSProjection) -> MapProjection
    {
        let mapProjection = GoogleMapProjection(projection: projection);
        return mapProjection;
    }
}
