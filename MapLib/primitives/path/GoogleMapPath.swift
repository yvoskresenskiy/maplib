//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//


import Foundation
import GoogleMaps

class GoogleMapPath: MapPath {
    
    let path:GMSMutablePath;
    
    required init(){
        path = GMSMutablePath();
    }
    
    open func count() -> UInt {
        return path.count()
    }
    
    open func coordinateAtIndex(_ index: UInt) -> CLLocationCoordinate2D {
        return path.coordinate(at: index)
    }
    
    open func addCoordinate(_ coord: CLLocationCoordinate2D) {
        path.add(coord)
    }
}
