//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps

open class MapPathBuilder
{
    open static func buildGoogleMap() -> MapPath
    {
        let path:GoogleMapPath = GoogleMapPath();
        
        return path;
    }
}
