//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


open class MapCoordinatesBoundsBuilder
{
    open static func buildCoordinatesBounds(coordinate coord1: CLLocationCoordinate2D, coordinate coord2: CLLocationCoordinate2D) -> MapCoordinatesBounds
    {
        let bounds = GoogleMapCoordinatesBounds(coordinate: coord1, coordinate: coord2);
        return bounds;
    }
    
    internal static func buildGoogleMapCoordinatesBounds(region boundsValue: GMSVisibleRegion) -> MapCoordinatesBounds
    {
        let bounds = GoogleMapCoordinatesBounds(region: boundsValue)
        return bounds
    }
}
