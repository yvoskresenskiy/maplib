//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


class GoogleMapCoordinatesBounds: MapCoordinatesBounds {
    
    fileprivate let bounds:GMSCoordinateBounds
    
    init(region: GMSVisibleRegion) {
        self.bounds = GMSCoordinateBounds(region: region)
    }
    
    init(coordinate coord1: CLLocationCoordinate2D, coordinate coord2: CLLocationCoordinate2D) {
        self.bounds = GMSCoordinateBounds(coordinate: coord1, coordinate: coord2)
    }
    
    var nearLeft: CLLocationCoordinate2D
    {
        get {
            return self.bounds.southWest
        }
    }
    
    var farRight: CLLocationCoordinate2D
    {
        get {
            return self.bounds.northEast
        }
    }
    
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
    {
        return bounds.contains(coordinate)
    }
    
}
