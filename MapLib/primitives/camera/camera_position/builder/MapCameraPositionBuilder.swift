//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


open class MapCameraPositionBuilder
{
    static func buildGoogleCameraPosition(_ gmsCameraPosition:GMSCameraPosition) -> MapCameraPosition
    {
        let googleMapCameraPosition:GoogleMapCameraPosition = GoogleMapCameraPosition(target: gmsCameraPosition.target, zoom: gmsCameraPosition.zoom, bearing: gmsCameraPosition.bearing, viewingAngle: gmsCameraPosition.viewingAngle);
        
        return googleMapCameraPosition;
    }
}
