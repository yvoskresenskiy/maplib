//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


public class GoogleMapEngine: NSObject, MapEngine
{
    //MARK: IMapEngine
    open func startConfiguration(apiKey: String)
    {
        GMSServices.provideAPIKey(apiKey)
    }
}
