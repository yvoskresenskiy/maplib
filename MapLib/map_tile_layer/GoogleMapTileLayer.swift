//
//  MapLib.h
//  MapLib
//
//  Created by Юрий Воскресенский on 28.09.17.
//  Copyright © 2017 Юрий Воскресенский. All rights reserved.
//

import Foundation
import GoogleMaps


class GoogleMapTileLayer: GMSTileLayer, MapTileLayer
{
    weak var delegate: MapTileLayerDelegate?
    
    public override func requestTileFor(x: UInt, y:UInt, zoom:UInt, receiver: GMSTileReceiver)
    {
        let cell = TileCell(column: x, row: y, zoom: zoom)
        
        delegate?.requestTileFor(cell: cell)
        {
            receivedImage in
            receiver.receiveTileWith(x: x, y: y, zoom: zoom, image: receivedImage)
        }
    }
    
    public override func clearTileCache()
    {
        super.clearTileCache()
        delegate?.clearTileCache()
    }
    
    public func removeFromMap() {
        self.map = nil
    }
    
    // It is strange, but Google Maps has pt which diffs from CoreGraphics pt.
    // By default in google maps the size of tile is 256pt, but really this size is same 160 CG pt.
    // So, 1 GoogleMap's pt = 1.6 CG pt for iPhone 5S, iPhone 6S
    // and 1 GoogleMap's pt = 2.0 CG pt for iPhone 6S Plus
    let googleMapPT:CGFloat = UIDevice().type.rawValue.contains("Plus") ? 2.0 : 1.6
    public var sizeOfTile: CGFloat {
        get {
            return CGFloat(self.tileSize) / googleMapPT
        }
        set {
            self.tileSize = Int(newValue * googleMapPT)
        }
    }
}
